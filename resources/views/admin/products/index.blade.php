@extends('admin.layouts')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h1>Product Catalog</h1>
                    <div class="row">
                        <div class="col-sm-6">
                            <h5><!-- insert title here --></h5>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a href="{{ route('admin.products.create') }}" class="btn btn-light btn-sm">
                                @lang('crud.create_button')
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    {!! Form::model($products, ['action' => 'ProductController@search', 'method'=> 'GET']) !!}
                    <div class="form-group">
                        {!! Form::text('query', '', ['placeholder' => 'search','class'=>'form-control']) !!}
                    </div>
                    <div class="card-footer">

                        <div class="row">
                            <div class="col-sm-6 text-right">
                                {!! Form::submit('Search', ['class'=>'btn btn-success']) !!}
                            </div>

                        </div>

                    </div>
                    {!! Form::close() !!}
                    @include('crudable::notifications')
                    @if($products->isEmpty())
                    @lang('crud.no_entries')
                    @else
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $product)

                            <tr>
                                <td> <a href="{{ route('admin.products.show',$product->id) }}">{{$product->name}}</a></td>
                                <td class="text-right">
                                    <div class="btn-group" role="group">
                                        <form class="btn-group"
                                            action="{{ route('admin.products.destroy',$product->id) }}"
                                            method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button class="btn btn-sm btn-danger"
                                                    type="submit"><i class="glyphicon glyphicon-trash"></i> @lang('crud.delete')</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@stop
