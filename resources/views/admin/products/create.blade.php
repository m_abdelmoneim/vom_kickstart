@extends('admin.layouts')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <h1>Create new Product</h1>
                @include('crudable::notifications')
                <div class="card">
                    {!! Form::model($product, ['action' => 'ProductController@store']) !!}
                    <div class="form-group">
                        {!! Form::label('name', 'Name') !!}
                        {!! Form::text('name', '', ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('price', 'Price') !!}
                        {!! Form::number('price', '', ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('category_id', 'Category') !!}
                        {!!  Form::select('category_id', $categories, null,['class' => 'form-control'])  !!}
                    </div>


                    <div class="card-footer">

                        <div class="row">
                            <div class="col-sm-6 text-right">
                                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                            </div>

                        </div>

                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
@stop
