<?php

namespace App\Repositories;

use App\Models\Category;
use Flobbos\Crudable\Contracts\Crud;
use Flobbos\Crudable;

class CategoryService implements Crud {

    use Crudable\Crudable;

    public function __construct(Category $category) {
        $this->model = $category;
    }

    public function pluck(...$params){

        return $this->model->pluck(...$params);
    }

}
