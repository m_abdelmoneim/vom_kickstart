<?php

namespace App\Repositories;

use App\Models\Product;
use Flobbos\Crudable\Contracts\Crud;
use Flobbos\Crudable;

class ProductService implements Crud {

    use Crudable\Crudable;

    public function __construct(Product $product) {
        $this->model = $product;
    }

    public function pluck(...$params){

        return $this->model->pluck(...$params);
    }



}
