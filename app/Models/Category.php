<?php

namespace App\Models;

use App\Repositories\ProductService;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name'
    ];



    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
