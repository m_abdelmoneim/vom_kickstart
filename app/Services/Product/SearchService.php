<?php


namespace App\Services\Product;


use App\Repositories\ProductService;
use Illuminate\Support\Facades\Validator;

class SearchService
{
        private $repo = null;

        public function __construct(ProductService $repo)
        {
            $this->repo = $repo;
        }


        public function execute()
        {
            $query = \request()->get('query');
            $products = $this->repo->where('name' , 'like' , "%$query%")->get();
           return $products;
        }
}
