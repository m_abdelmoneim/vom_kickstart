<?php


namespace App\Services\Product;


use App\Repositories\ProductService;
use Illuminate\Support\Facades\Validator;

class CreatingProductService
{
        private $repo = null;

        public function __construct(ProductService $repo)
        {
            $this->repo = $repo;
        }


        public function execute()
        {
            $data =  $this->repo->create(\request()->all());

            return redirect()->route('admin.products.index');
;
        }
}
