<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Repositories\CategoryService;
use App\Repositories\ProductService;
use App\Services\Product\CreatingProductService;
use App\Services\Product\SearchService;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class ProductController extends Controller {

    protected $repo;

    public function __construct(ProductService $repo) {
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index(){
        $products = $this->repo->get();
        return view('admin.products.index', compact('products'));
    }


    /**
     * Display a listing of the resource.
     *
     */
    public function search(SearchService $service){
        $products = $service->execute();
        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create(CategoryService $service){

        $product = new Product;
        $categories = $service->pluck('name', 'id');

        return view('admin.products.create', compact('product', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     *
     */
    public function store(CreatingProductService $service, StoreProductRequest $request){
        return  $service->execute();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     */
    public function show($id){
        return view('admin.products.show')->with(['product'=>$this->repo->get($id)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     */
    public function destroy($id){
        try{
            $this->repo->delete($id);
            return redirect()->route('admin.products.index')->withMessage(trans('crud.record_deleted'));
        } catch (Exception $ex) {
            return redirect()->route('admin.products.index')->withErrors($ex->getMessage());
        }
    }
}

